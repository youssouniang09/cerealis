import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';


import { SharingPage } from './sharing.page';
import { FormsModule, ReactiveFormsModule }  from '@angular/forms';
import {SharingPageRoutingModule} from './sharing-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharingPageRoutingModule,
  ],
  declarations: [SharingPage]
})
export class SharingPageModule {}

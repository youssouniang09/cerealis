import { Component, OnInit } from '@angular/core';
import { ActionSheetController, Platform } from '@ionic/angular';
import { Screenshot } from '@ionic-native/screenshot/ngx';
import { NavController, AlertController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ConnexionPage } from '../connexion/connexion.page';
import {SharingPage} from '../sharing/sharing.page';

@Component({
  selector: 'app-visualisation',
  templateUrl: 'visualisation.page.html',
  styleUrls: ['visualisation.page.scss']
})
export class VisualisationPage implements OnInit {
  displayCamera: boolean;
  displayAlertMissing: boolean;
  displayShareButton: boolean;
  userMessage: string;
  source: string;

  constructor(private alertController: AlertController,
    private router: Router,
              public actionSheetController: ActionSheetController,
              public screenshot: Screenshot,
              private navCtrl: NavController,
              private modalCtrl: ModalController,
              public platform: Platform
  ) {}

  async ngOnInit() {
    this.displayCamera = true;
    this.displayAlertMissing = true;
    this.displayShareButton = false;
    this.userMessage = 'Place the camera above the drawing';
    this.initErrorMessageLoader();
    console.log(this.platform.platforms());
    //registerWebPlugin(FileSharer);
  }

  async openModal(){
    const modal = await this.modalCtrl.create({
      component: ConnexionPage,
      cssClass: 'app-connexion-page'
    });
    modal.onDidDismiss().then(data => {
      // Call the method to do whatever in your home.ts
      this.source = data.data.dismissvalue;
      if(this.source === 'submit') {
        this.showShareOptions();
      }
    });
    return await modal.present();
  }

  async showShareOptions() {
    const modal = await this.modalCtrl.create({
      component: SharingPage,
      cssClass: 'backTransparent',
      backdropDismiss: true
    });
    return modal.present();
  }

  public initErrorMessageLoader() {

    window.addEventListener('message', handleMessage, false);

    const self = this;

    // eslint-disable-next-line prefer-arrow/prefer-arrow-functions
    function handleMessage(event) {
      if (event.data === 'IS_VISIBLE') {
        self.displayAlertMissing = false;
        self.displayShareButton = true;
        console.log(self.displayShareButton);
      } else if (event.data === 'IS_NOT_VISIBLE') {
        self.displayAlertMissing = true;
        self.displayShareButton = false;
        self.userMessage = 'I no longer see your drawing';
      }
    }
  }


  takeScreenshotURI() {
    this.screenshot.URI(30).then(res => {
      const uri = res.URI;
      console.log(uri);
      // this.shareScreenshot(uri);
    });
  }

  navigate(){
    this.openModal();
  }
}

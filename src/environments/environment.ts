// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  socialShareOption: [
    {
      title: 'Whatsapp',
      logo: 'assets/socialShare/whatsapp-icon-280x280.png',
      shareType: 'shareViaWhatsApp'
    },
    {
      title: 'Facebook',
      logo: 'assets/socialShare/facebook.png',
      shareType: 'shareViaFacebook'
    },
    {
      title: 'Twitter',
      logo: 'assets/socialShare/twitter.png',
      shareType: 'shareViaTwitter'
    },
    {
      title: 'Instagram',
      logo: 'assets/socialShare/Instagram-circle.png',
      shareType: 'shareViaInstagram'
    },
    {
      title: 'Email',
      logo: 'assets/socialShare/mail.png',
      shareType: 'viaEmail'
    }
  ],
  socialShareWebOption: [
    {
      title: 'Facebook',
      logo: 'assets/socialShare/facebook.png',
      href: 'https://facebook.com/sharer/sharer.php?u=http://farm8.staticflickr.com/7027/6851755809_df5b2051c9_z.jpg'
    },
    {
      title: 'Twitter',
      logo: 'assets/socialShare/twitter.png',
      // eslint-disable-next-line max-len
      href: 'https://twitter.com/intent/tweet/?text=Super%20fast%20and%20easy%20Social%20Media%20Sharing%20Buttons.%20No%20JavaScript.%20No%20tracking.&url=http://farm8.staticflickr.com/7027/6851755809_df5b2051c9_z.jpg'
    },
    {
      title: 'Pinterest',
      logo: 'assets/socialShare/pinterest.png',
      // eslint-disable-next-line max-len
      href: 'https://pinterest.com/pin/create/link/?url=http://farm8.staticflickr.com/7027/6851755809_df5b2051c9_z.jpg&media=http://farm8.staticflickr.com/7027/6851755809_df5b2051c9_z.jpg&description=Super%20fast%20and%20easy%20Social%20Media%20Sharing%20Buttons.%20No%20JavaScript.%20No%20tracking.'
    },
    {
      title: 'Email',
      logo: 'assets/socialShare/mail.png',
      // eslint-disable-next-line max-len
      href: 'mailto:receipient@user.com?subject=Super%20fast%20and%20easy%20Social%20Media%20Sharing%20Buttons.%20No%20JavaScript.%20No%20tracking.&body=This is an email body shared directly from an Ionic app using URL or links. This can be used in any web app.'
    }
  ]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

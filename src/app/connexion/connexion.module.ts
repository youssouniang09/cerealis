import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';


import { ConnexionPage } from './connexion.page';
import { FormsModule, ReactiveFormsModule }  from '@angular/forms';
import {ConnexionPageRoutingModule} from './connexion-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ConnexionPageRoutingModule,
  ],
  declarations: [ConnexionPage]
})
export class ConnexionPageModule {}

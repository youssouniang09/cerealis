import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VisualisationPage } from './visualisation.page';

const routes: Routes = [
  {
    path: '',
    component: VisualisationPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisualisationPageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { collection, addDoc, getDocs } from 'firebase/firestore';
import { getAnalytics } from 'firebase/analytics';
import { getDatabase, ref, set  } from 'firebase/database';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {ModalController} from '@ionic/angular';

const firebaseConfig = initializeApp({
  apiKey: 'AIzaSyCcJGpM4i_YnXjU3iHsZKdu8q45QjKMzPY',
  authDomain: 'cerealis-1.firebaseapp.com',
  databaseURL: 'https://cerealis-1-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'cerealis-1',
  storageBucket: 'cerealis-1.appspot.com',
  messagingSenderId: '1081577746662',
  appId: '1:1081577746662:web:2558cdc16eba61ad3e0b27',
  measurementId: 'G-SB93QEWK2M'
});

const analytics = getAnalytics(firebaseConfig);
const db = getFirestore();
const database = getDatabase();

@Component({
  selector: 'app-connexion-page',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {
  public identificationForm: FormGroup;
  public isSubmitted = false;
  public exist = false;

  constructor(private activatedRoute: ActivatedRoute,
              public formBuilder: FormBuilder, private router: Router,
              private modalCtrl: ModalController,) {
    this.identificationForm = this.formBuilder.group({
      email: ['', Validators.required],
      firstname: ['', Validators.required],
    });
  }

  ngOnInit() {}

  submitForm() {
    this.isSubmitted = true;
    if (!this.identificationForm.valid) {
      console.log('Formulaire non valide');
      return false;
    }
    else  {
      this.control();
    }
  }

  control() {
    this.exist = false;
    (async () => {
      const querySnapshot = await getDocs(collection(db, 'users'));
      querySnapshot.forEach((doc) => {
        if(this.identificationForm.controls.email.value === doc.data().email
          ) {
          this.exist = true;
          console.log(`${doc.id} => ${doc.data().email} ${doc.data().prenom}`);
        }
      });
      if(!this.exist) {
        const docRef = await addDoc(collection(db, 'users'), {
          firstname: this.identificationForm.controls.firstname.value,
          email: this.identificationForm.controls.email.value
        });
        console.log('Enregistrement effectué : ', docRef.id);
        this.closeModal('submitt');
      } else {
        console.log('Enregistrement existant');
        this.closeModal('submit');
      }
    })();
  }

  closeModal(source: string){
    this.modalCtrl.dismiss({
      dismissvalue: source,
      dismissed: true
    });
  }

}

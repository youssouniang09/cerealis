import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { VisualisationPage } from './visualisation.page';
import { ConnexionPageModule } from '../connexion/connexion.module';
import { SharingPageModule } from '../sharing/sharing.module';

import { VisualisationPageRoutingModule } from './visualisation-routing.module';

@NgModule({
  imports: [
    IonicModule.forRoot(),
    CommonModule,
    FormsModule,
    VisualisationPageRoutingModule,
    ConnexionPageModule,
    SharingPageModule
  ],
  declarations: [VisualisationPage],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class VisualisationPageModule {}

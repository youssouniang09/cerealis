import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'cerealis.app',
  appName: 'cerealis',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;

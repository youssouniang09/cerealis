import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { ModalController, Platform } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

type CurrentPlatform = 'browser' | 'native';

@Component({
  selector: 'app-sharing-page',
  templateUrl: './sharing.page.html',
  styleUrls: ['./sharing.page.scss'],
})
export class SharingPage implements OnInit {
  public sharingList: any;
  loader: any = null;
  // eslint-disable-next-line max-len
  sharingText = 'You can download our app from playstore or use this link to download the app. And you can share awesome coupons with your loved once, Thank you';
  emailSubject = 'Download Apps';
  recipent = ['youssouniang09gmail.com'];
  sharingImage = ['https://store.enappd.com/wp-content/uploads/2019/03/700x700_2-1-280x280.jpg'];
  sharingUrl = 'https://store.enappd.com';
  private currentPlatform: CurrentPlatform;
  constructor(
    private modal: ModalController,
    private socialSharing: SocialSharing,
    public platform: Platform
  ) { }

  public setCurrentPlatform() {
    // Are we on mobile platform? Yes if platform is ios or android, but not desktop or mobileweb, no otherwise
    if (
      this.platform.is('ios')
      || this.platform.is('android')
      && !( this.platform.is('desktop') || this.platform.is('mobileweb') ) ) {
      this.currentPlatform = 'native';
    } else {
      this.currentPlatform = 'browser';
    }
  }

  isNative() {
    return this.currentPlatform === 'native';
  }
  isBrowser() {
    return this.currentPlatform === 'browser';
  }

  ngOnInit() {
    this.setCurrentPlatform();
    if(this.isNative()) {
      this.sharingList = environment.socialShareOption;
    } else if(this.isBrowser()) {
      this.sharingList = environment.socialShareWebOption;
    }
  }

  closeModal() {
    this.modal.dismiss();
  }

  async shareVia(shareData) {
    if (shareData.shareType === 'viaEmail') {
      this.shareViaEmail();
    } else {
      this.socialSharing[`${shareData.shareType}`](this.sharingText, this.sharingImage, this.sharingUrl)
        .then((res) => {
          this.modal.dismiss();
        })
        .catch((e) => {
          console.log('error', e);
          this.modal.dismiss();
        });
    }
  }
  shareViaEmail() {
    this.socialSharing.canShareViaEmail().then((res) => {
      this.socialSharing.shareViaEmail(this.sharingText, this.emailSubject, this.recipent, null, null, this.sharingImage).then(() => {
        this.modal.dismiss();
      });
    }).catch((e) => {
      // Error!
    });
  }

}
